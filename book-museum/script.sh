#!/bin/sh
RANDOM=$$
ROLE_NAME=book-museum-role-${RANDOM}

# install Node Libraries
npm install

# create deployment package
zip -r deployment-package.zip index.js node_modules

# create role that can be assumed by Lambda and get ARN
ROLE_ARN=$(aws iam create-role --role-name ${ROLE_NAME} ... --query 'Role.Arn' --output text && sleep 10)

# attach AWSLambdaBasicExecutionRole policy to the above role
aws iam attach-role-policy --role-name ${ROLE_NAME} ...

# deploy function
aws lambda create-function ...