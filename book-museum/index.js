const fetch = require("node-fetch");

exports.handler = async (payload) => {
   // fetch the request
   let response = await fetch("https://q6a0lma5rd.execute-api.eu-west-1.amazonaws.com/Prod/hotel", {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: { "Content-Type": "application/json" }
   });

   // get the JSON
   let json = await response.json();
   return json;
}
